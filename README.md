# Steps to describe getting let's encrypt working with duckdns and lede/openwrt using dns-01 mode

## Installing acme.sh script

### Package preruiqisites

```bash
opkg install luci-ssl-openssl curl ca-bundle
```

### Download and install acme.sh

```bash
curl https://raw.githubusercontent.com/Neilpang/acme.sh/master/acme.sh > acme.sh
chmod a+x "acme.sh"
./acme.sh --install
```

## Getting the Letsencrypt certs

### Run acme.sh script to capture the validation string

```bash
cd .acme.sh
DOMAIN=MYOWNSPECIAL.duckdns.org
./acme.sh --issue -d $DOMAIN --dns --yes-I-know-dns-manual-mode-enough-go-ahead-please
```

### Run below script to set the TXT record for the duckdns.org domain

```bash
#!/bin/ash
# Get your DNS Token from https://www.duckdns.org
TOKEN="get-token-from-duckdns.org-website"
DOMAIN="MYOWNSPECIAL.duckdns.org"
CERTBOT_VALIDATION="<captured-letsencrypt-validation-string>"

echo url="https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=$CERTBOT_VALIDATION&verbose=true" | curl -k -o /root/LetsEncrypt_DuckDNS_update-curl.log -K -
```

### Renew the certificate after the TXT record has been set

```bash
./acme.sh --renew -d $DOMAIN
```

## Further steps for using the certs

### For using the newly created cert for lede/openwrt run the following commands:

```bash
uci set uhttpd.main.key="$(pwd)/$DOMAIN/$DOMAIN.key"
uci set uhttpd.main.cert="$(pwd)/$DOMAIN/$DOMAIN.cer"
uci commit uhttpd
```

### To create a pfx certificate which can be used for Windows based services run the following commands:

```bash
openssl pkcs12 -export -out "$(pwd)/$DOMAIN/$DOMAIN.pfx" -inkey "$(pwd)/$DOMAIN/$DOMAIN.key" -in $(pwd)/$DOMAIN/$DOMAIN.cer" -certfile "$(pwd)/$DOMAIN/ca.cer"
```

## References:

https://www.splitbrain.org/blog/2017-08/10-homeassistant_duckdns_letsencrypt

https://github.com/ebekker/ACMESharp/wiki/Example-Usage

https://certbot.eff.org/docs/

https://github.com/Neilpang/acme.sh/wiki/How-to-run-on-OpenWRT

https://letsencrypt.org/docs/